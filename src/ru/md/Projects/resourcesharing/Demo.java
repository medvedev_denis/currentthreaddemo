package ru.md.Projects.resourcesharing;

public class Demo {

    public static void main(String[] args){
        ResourceSharing firstThread = new ResourceSharing("Поток 1");
        ResourceSharing secondThread = new ResourceSharing("Поток 2");
        System.out.println(Thread.currentThread().getName() + "; i: " + ResourceSharing.getI());
        firstThread.start();
        secondThread.start();
        try {
            firstThread.join();
            secondThread.join();
        } catch (InterruptedException e) {}
        System.out.printf("Ожидание: 100" +
                "\nПолучили: " + firstThread.getI());
    }

}
