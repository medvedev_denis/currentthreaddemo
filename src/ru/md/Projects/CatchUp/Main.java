package ru.md.Projects.CatchUp;

public class Main {

    public static void main(String[] args) {
        CatchUp firstThread = new CatchUp(10, "Первый поток");
        CatchUp secondThread = new CatchUp(1, "Второй поток");
        System.out.printf("Приоритет потока %s равен %d!\n", firstThread.getName(), firstThread.getPriority());
        System.out.printf("Приоритет потока %s равен %d!\n", secondThread.getName(), secondThread.getPriority());
        firstThread.start();
        secondThread.start();
        try {
            firstThread.join();
            secondThread.join();
        } catch (InterruptedException e){}
        System.out.println("Завершение!!!");
    }
}