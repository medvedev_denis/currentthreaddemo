package ru.md.Projects.CatchUp;

public class CatchUp extends Thread {

    private int threadPriority;
    private int counter = 0;


    public CatchUp (int number, String name) {
        this.threadPriority = number;
        this.setName(name);
    }

    public void run() {
        System.out.println(this.getName() + " запущен!");
        while (counter < 300) {
            counter++;
            System.out.printf("%s: %d;\n", this.getName(), counter);
            switch (counter){
                case (100):
                    this.setPriority(threadPriority);
                    System.out.printf("Приоритет потока %s изменен на %d!\n", this.getName(), this.getPriority());
                    break;
                case (200):
                    this.setPriority(11 - threadPriority);
                    System.out.printf("Приоритет потока %s изменен на %d!\n", this.getName(), this.getPriority());
                    break;
                case (300):
                    System.out.println(this.getName() + " завершен!");
                default:
                    continue;
            }
        }
    }
}
