package ru.md.Projects.CopyFile;

import java.io.*;

public class FileWork extends Thread{

    private String needCopy;
    private String copyResult;

    public FileWork(String copyFile, String resultFile){
        this.needCopy = copyFile;
        this.copyResult = resultFile;
    }

    public void run() {
        String copyString;
        System.out.println(this.getName() + " начал работу с файлами");
        try (BufferedReader br = new BufferedReader(new FileReader(needCopy))){
            BufferedWriter bw = new BufferedWriter(new FileWriter(copyResult));
            copyString = br.readLine();
            while (copyString!=null){
                bw.write(copyString + "\n");
                copyString = br.readLine();
            }
            bw.close();
        } catch (IOException e) {
            System.out.println("Ошибка при копировании файлов!");
        }
        System.out.println(this.getName() + " закончил работу с файлами");
    }
}