package ru.md.Projects.CopyFile;

public class CopyFile {

    private static final String FIRST_FILE = "src\\ru\\md\\Projects\\CopyFile\\FirstCopyFile.txt";
    private static final String SECOND_FILE = "src\\ru\\md\\Projects\\CopyFile\\SecondCopyFile.txt";
    private static final String FIRST_COPY_FIRST_FILE = "src\\ru\\md\\Projects\\CopyFile\\FirstConcurrentExecutionResultCopyFile.txt";
    private static final String SECOND_COPY_FIRST_FILE = "src\\ru\\md\\Projects\\CopyFile\\FirstConsistentExecutionResultCopyFile.txt";
    private static final String FIRST_COPY_SECOND_FILE = "src\\ru\\md\\Projects\\CopyFile\\SecondConcurrentExecutionResultCopyFile.txt";
    private static final String SECOND_COPY_SECOND_FILE = "src\\ru\\md\\Projects\\CopyFile\\SecondConsistentExecutionResultCopyFile.txt";

    public static void main(String[] args) {
        concurrentExecution();
        consistentExecution();
    }

//    параллельный запуск потоков
    private static void concurrentExecution(){
        FileWork firstThread = new FileWork(FIRST_FILE, FIRST_COPY_FIRST_FILE);
        FileWork secondThread = new FileWork(SECOND_FILE, FIRST_COPY_SECOND_FILE);
        long before = System.currentTimeMillis();
        firstThread.start();
        secondThread.start();
        try {
            firstThread.join();
            secondThread.join();
            System.out.printf( "Время работы потоков %s и %s: %d мс\n", firstThread.getName(), secondThread.getName(), (System.currentTimeMillis() - before));
        } catch (InterruptedException e) {
            System.out.printf("Поток %s или %s прерван\n", firstThread.getName(), secondThread.getName());
            System.out.printf( "Время работы потоков %s и %s: %d мс\n", firstThread.getName(), secondThread.getName(), (System.currentTimeMillis() - before));
        }
    }

//    последовательный запуск потоков
    private static void consistentExecution(){
        FileWork firstThread = new FileWork(FIRST_FILE, SECOND_COPY_FIRST_FILE);
        FileWork secondThread = new FileWork(SECOND_FILE, SECOND_COPY_SECOND_FILE);
        long beforeFirstThread = System.currentTimeMillis();
        long afterFirstThread = 0;
        firstThread.start();
        try {
            firstThread.join();
            afterFirstThread = System.currentTimeMillis() - beforeFirstThread;
            System.out.printf("Время работы %s: %d мс\n", firstThread.getName(), afterFirstThread);
        } catch (InterruptedException e) {
            System.out.printf("Поток %s прерван", firstThread.getName());
            System.out.printf("Время работы %s: %d мс", firstThread.getName(), afterFirstThread);
        }
        long beforeSecondThread = System.currentTimeMillis();
        long afterSecondThread = 0;
        secondThread.start();
        try {
            secondThread.join();
            afterSecondThread = System.currentTimeMillis() - beforeSecondThread;
            System.out.printf("Время работы %s: %d мс\n", secondThread.getName(), afterSecondThread);
        } catch (InterruptedException e) {
            System.out.printf("Поток %s прерван", secondThread.getName());
            System.out.printf("Время работы %s: %d мс\n", secondThread.getName(), afterSecondThread);
        }
        System.out.printf("Время работы потоков %s и %s: %d мс", firstThread.getName(), secondThread.getName(), (afterFirstThread + afterSecondThread));
    }
}