package ru.md.Projects.ReadingData;

import java.io.FileReader;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.util.concurrent.Semaphore;

public class FileWork extends Thread{

    private String fileName;
    private BufferedWriter bw;

    public FileWork(String fileName, BufferedWriter bufferedWriter){
        this.bw = bufferedWriter;
        this.fileName = fileName;
    }

    public void run() {
        Semaphore sem = new Semaphore(1);
        String readingString = "";
        System.out.println(this.getName() + " начал работу с файлами");
        try {
            sem.acquire();
            try(BufferedReader br = new BufferedReader(new FileReader(fileName));) {
                while (true) {
                    readingString = br.readLine();
                    if (readingString==null){
                        break;
                    }
                    bw.write(readingString + "\n");
                    Thread.sleep(100);
                }
                br.close();
                bw.close();
            } catch (IOException e) {
                System.out.printf("Ошибка при работе файлами потока " + this.getName());
            }
            sem.release();
        } catch (InterruptedException e) {
            System.out.println("Ошибка при работе семафора в потоке " + this.getName());
        }
        System.out.println(this.getName() + " закончил работу с файлами");
    }
}
